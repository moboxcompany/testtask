<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Debts */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="debt">
    <div class="sum"><?=  $model->sum ?></div>
    <div><?=  $model->comment ?></div>
    <div class="del"><?= Html::a('Удалить', ['debts/deleteajax', 'id'=>$model->id], ['class' => 'deletedebt', 'data-id' => $model->id]) ?></div>
    <div class="clear"></div>
</div>
