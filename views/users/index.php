<?php

use app\models\Users;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\jui\AutoComplete;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UsersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Users';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="users-index">

    <p>
        <?php // Html::a('Create Users', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?php
    $form = ActiveForm::begin();

    $data = Users::find()
        ->select(['fio as value', 'id'])
        ->asArray()
        ->all();

echo  $form->field($model, 'fio')->widget(AutoComplete::classname(), [
        'name' => 'fio',
        'clientOptions' => [
            'source' => $data,
            'select' => new yii\web\JsExpression("function( event, ui ) {
                $('#debts-userid').val(ui.item.id);
            }"),
        ],
    ]);

    ?>


    <?= $form->field($debt, 'sum')->textInput(['maxlength' => true]) ?>
    <?= $form->field($debt, 'comment')->textarea(['rows' => 6, 'class'=>'form-control']) ?>
    <?= $form->field($debt, 'userid')->hiddenInput(['class' => 'hidden-helper'])->label('')?>

    <?= Html::submitButton('Создать' , ['class' => 'btn btn-primary btn-lg']) ?>

    <?php ActiveForm::end(); ?>



    <div class="results">
        <?php
        foreach($users as $us){
        ?>
        <div class="us" id="<?= 'id'.$us->id ?>">
            <div class="left">
                <p><?= $us->fio ?></p>
            </div>
            <div class="right">
            <?php
            foreach($us->debts as $deb){
                echo $this->render('_debt', [
                    'model'=> $deb,
                ]);
            }
            ?>
            </div>
            <div class="clear"></div>
        </div>

        <?php
        }
        ?>
    </div>
</div>
