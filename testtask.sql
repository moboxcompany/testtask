-- phpMyAdmin SQL Dump
-- version 4.0.10.7
-- http://www.phpmyadmin.net
--
-- Хост: localhost:3306
-- Час створення: Гру 02 2015 р., 20:03
-- Версія сервера: 5.5.46-cll
-- Версія PHP: 5.4.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База даних: `chiskiev_testtask`
--

-- --------------------------------------------------------

--
-- Структура таблиці `debts`
--

CREATE TABLE IF NOT EXISTS `debts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) DEFAULT NULL,
  `sum` int(11) DEFAULT NULL,
  `comment` text,
  `created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_debts_users` (`userid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=36 ;

--
-- Дамп даних таблиці `debts`
--

INSERT INTO `debts` (`id`, `userid`, `sum`, `comment`, `created`) VALUES
(1, 5, 12333, 'Много денег', '2015-12-02 16:01:16'),
(11, NULL, NULL, '', '2015-12-02 16:39:03'),
(12, NULL, NULL, '', '2015-12-02 16:39:03'),
(13, NULL, NULL, '', '2015-12-02 16:39:05'),
(14, NULL, NULL, '', '2015-12-02 16:39:07'),
(15, NULL, NULL, '', '2015-12-02 16:39:07'),
(33, 1, 100500, 'Новый долг', '2015-12-02 17:47:23'),
(34, 2, 100500000, 'Новый большой долг', '2015-12-02 17:48:25'),
(35, 2, 99, 'Маленький долг', '2015-12-02 17:48:40');

-- --------------------------------------------------------

--
-- Структура таблиці `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fio` varchar(255) DEFAULT NULL,
  `created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Дамп даних таблиці `users`
--

INSERT INTO `users` (`id`, `fio`, `created`) VALUES
(1, 'Червоноглаз Юрий Николаевич', '2015-12-02 13:45:11'),
(2, 'Свистопляс Никита Сергеевич', '2015-12-02 13:45:11'),
(5, 'Декабрь Свирид Петрович', '2015-12-02 13:46:09'),
(6, 'Мироточин Илья Александрович', '2015-12-02 13:46:09');

--
-- Обмеження зовнішнього ключа збережених таблиць
--

--
-- Обмеження зовнішнього ключа таблиці `debts`
--
ALTER TABLE `debts`
  ADD CONSTRAINT `fk_debts_users` FOREIGN KEY (`userid`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
