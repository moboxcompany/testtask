<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "debts".
 *
 * @property integer $id
 * @property integer $userid
 * @property integer $sum
 * @property string $comment
 * @property string $created
 *
 * @property Users $user
 */
class Debts extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'debts';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['userid', 'sum'], 'integer'],
            [['comment'], 'string'],
            [['comment', 'sum'], 'required',
                'message'=>'Поле {attribute} не может быть пустым.'],
            [['created'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'userid' => 'Userid',
            'sum' => 'Сумма',
            'comment' => 'Комментарий',
            'created' => 'Создан',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'userid']);
    }
}
