/*
 *
 *   INSPINIA - Responsive Admin Theme
 *   version 2.0
 *
 */
var count = count||0;
$(document).ready(function () {

    /*-Submit Form-*/
    $('#w0').on('beforeSubmit', function(e){
        e.preventDefault();  // Полная остановка происходящего
        var msg   = $(this).serialize();

//        console.log($(this).find('#debts-sum').val());
//        console.log($(this).find('#debts-comment').val());


        if($(this).find('#debts-sum').val() != '' && $(this).find('#debts-comment').val() != '' ){
            $.ajax({
                type: 'POST',
                url: 'index.php?r=debts/createajax',
                data: msg,
                success: function(data) {
                    $('.results #id'+$('#debts-userid').val()+' .right').append(data);
                    return false;
                },
                error:  function(xhr, str){
                    alert('Возникла ошибка: ' + xhr.responseCode);
                }
            });
            return false;
        }

    });


    /*-Delete Debt-*/
    $('body').on('click', '.deletedebt', function(e){
        e.preventDefault();  // Полная остановка происходящего
        var href   = $(this).attr('href');
        var dat = $(this).attr('data-id');
        var par = $(this).parents('.debt');
        $.ajax({
            type: 'POST',
            url: href,
            data: 'id='+dat,
            success: function(data) {
                par.fadeOut();
            },
            error:  function(xhr, str){
                alert('Возникла ошибка: ' + xhr.responseCode);
            }
        });
        return false;

    });


});

